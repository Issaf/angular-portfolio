import { Component, OnInit } from '@angular/core';
import { EasingLogic } from 'ngx-page-scroll';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  showText: boolean = false;
  constructor() { }

  ngOnInit() {
    this.showText = true;
  }

  atTopOfPage() {
    if (window.scrollY === 0) {
      return true;
    } else {
      return false;
    }
  }

  easeInOutExpo: EasingLogic = {
    ease: (t: number, b: number, c: number, d: number): number => {
        // easeInOutExpo easing
        if (t === 0) return b;
        if (t === d) return b + c;
        if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    }
};
}
