import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  // Project Template
  // 0: {
  //   bgImg: '',
  //   title: '',
  //   stack: ['', '', ''],
  //   description: `
  //    `,
  //   urlType: '',
  //   url: ''
  // },
  

  // ========================
  // P R O J E C T S  L I S T
  // ========================
  projects = {
    0: {
      bgImg: '../../assets/images/cover-mvmtribe.jpg',
      title: 'MVMTRIBE',
      stack: ['Node.js', 'Angular 6', 'PostgreSQL'],
      description: `
      A fitness platform. Built both back and front end, 
      designed the UI (fully responsive) and DB architecture. Used several 
      services such as Mapbox, AWS S3 and Nginx. note: temporary testing url.
      `,
      urlType: 'external',
      url: 'https://www.mvmtribe.com'
      },
    1: {
      bgImg: '../../assets/images/cover-mtlrents.jpeg',
      title: 'MTLRents',
      stack: ['Python', 'Django', 'Bootstrap'],
      description: `
      A web app using scrapped data to estimate rent prices in Montreal, Canada. 
      The main goal of this project is to use the generated estimate to analyse 
      real estate investments.`,
      urlType: 'external',
      url: 'https://mtlrents.ca'
    },
    2: {
      bgImg: '../../assets/images/cover-catalog-marketing.jpeg',
      title: 'Catalog Marketing',
      stack: ['Python', 'Jupyter Notebook'],
      description: `
      Analysing a catalog marketing strategy and predicting the expected profit. 
      Pandas, Sklearn, Matlibplot, and Seaborn were the main libraries used.
      `,
      urlType: 'external',
      url: 'https://github.com/issaf/catalog_marketing/blob/master/analysis.ipynb'
    },
    3: {
      bgImg: '../../assets/images/cover-openstreetmap.jpeg',
      title: "What's good in Montreal?",
      stack: ['OpenStreetMap', 'Python', 'SQL'],
      description: `
      Perfomed some data wrangling using python and EDA using SQL on the OpenStreetMap 
      XML dataset for the city of Montreal, Canada.
      `,
      urlType: 'external',
      url: 'https://github.com/issaf/openstreetmap-data-analysis/blob/master/openstreetmap-project.ipynb'
    },
    4: {
      bgImg: '../../assets/images/cover-flight-delays-analysis.jpeg',
      title: 'Flight Delays Analysis',
      stack: ['Tableau', 'Python'],
      description: `
      A quick analysis of 2008 US flight delays using Tableau.
      After some data cleaning and formatting with python, I made a story illustrating 
      different factors that could cause delays.`,
      urlType: 'external',
      url: 'http://nbviewer.jupyter.org/github/issaf/flight_delays_analysis/blob/f41c067c221305ff378c9b7c6f68cececf0507b8/flight-delays-analysis.ipynb'
    },
  }
// ================================================================================


  keys = Object.keys(this.projects).sort();
  showProject: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  switch(event: number) {
    this.showProject = false;
    setTimeout(() => { 
      this.showProject = true;

    }, 50);
  }
}
