import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  email = "safsaf.islam@gmail.com";
  showNotif: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  copiedInfo() {
    this.showNotif = true;
    setTimeout(() => { this.showNotif = false }, 1700);
  }
}
